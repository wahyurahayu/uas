import { StackActions } from '@react-navigation/native';
import React, {useEffect} from 'react';
import { View, Text,Image} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';



interface SplashProps {
    navigation: any;
  }

const Splash = (props:SplashProps) => {
    useEffect(()=> {
        setTimeout(() => {
             props.navigation.dispatch(StackActions.replace('Awal'))
        }, 3000)
    })
    return (
        <ScrollView>
        <View style={{justifyContent:'center',alignItems:'center',height:870,backgroundColor:'#7C99AC'}}>
            <View style={{justifyContent:'center',alignItems:'center',
                height:250,
                width: 250,
                }}>           
            <Image source={require('../gambar/aaa-removebg-preview.png')}
            style={{
                width:200,
                height:200,
                }}></Image>
            </View>
        </View>
        </ScrollView>
    );
};

export default Splash;