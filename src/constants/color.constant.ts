const Colors = {
  white: '#fff',
  purple1: '#6998AB',
  purple2: '#22577E',
  purple3: '#CDDEFF',
  grey1: '#CEE5D0',
  grey2: '#1A374D',
  grey3: '#B1D0E0',
  line: '#6998AB',
  black:'#000000'
};

export default Colors;
